package com.sigmotoa.characters

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Recupera los datos de la fuente de datos
        val signatur = DataSource(this).getCharacterList()
        val score = DataSource(this).getCharacterList2()

        val recyclerView:RecyclerView=findViewById(R.id.reciclerview)
        //recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter=CharacterAdapter(signatur, score)
    }
}