package com.sigmotoa.characters
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

class CharacterAdapter(val signatur:Array<String>,val score:Array <String> ): RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>()
{
    class CharacterViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {

        val characterTextView:TextView=itemView.findViewById(R.id.character_text)
        val characterTextView2:TextView=itemView.findViewById(R.id.character_text2)
        fun bind (word: String, word2 : String){
            characterTextView.text =word
            characterTextView2.text =word2
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterAdapter.CharacterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.character_item, parent, false)
        return CharacterViewHolder(view)
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bind(signatur[position], score[position])
    }

    // Retorna el tamaño de la lista
    override fun getItemCount(): Int {
        return signatur.size
    }
}










